package com.newflypig.jblog.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import com.newflypig.jblog.model.Article;

/**
 *	定义关于Article类的数据库特别操作
 *	time：2015年11月28日
 *
 */
public interface IArticleDAO extends IBaseDAO<Article>{
	/**
	 * 倒序排列所有文章
	 * @return 倒序排列的Article有序列表
	 */
	public List<Article> findAllDesc();

	public void deleteForeverById(Integer articleId);

	public void ratePP(Integer articleId);

	public void setState(Integer articleId,Short state);

	public void callNoResultableProc(String procName);
	
	/**
	 * 为RSS提供数据源，加上前30条的限制
	 * @param setResultTransformer
	 * @return 数据源
	 */
	public List<Article> findForRSS(DetachedCriteria setResultTransformer);

	public void exchangeIndex(Integer theId, Integer otherId);

	public void updateIndex(Integer articleId);	
}
